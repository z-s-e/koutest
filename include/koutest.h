/* Copyright 2021 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef KOUTEST_H
#define KOUTEST_H

#include <atomic>

#ifdef KOUTEST_STD_STRING_HANDLING
#include <string>
#endif

namespace koutest {

    struct callbacks { // To register your custom callbacks, create a global instance of your subclass
        callbacks();

        virtual void begin_test_case(const char* name, int series_index);
        virtual void end_test_case();
        virtual void begin_subtest(const char* name);
        virtual void end_subtest();

        virtual void check_failed(const char* exp);
        virtual void check_cmp_failed(const char* lhs, const char* rhs, const char* lhs_exp, const char* rhs_exp, const char* op);

        virtual void exception(const char* what, const char* last_check_file, int last_check_line);

        // Note: the following method is called from a signal handler, so be careful what you do in there
        virtual void signal(int sig, const char* signame, const char* last_check_file, int last_check_line);
    };

    struct global_init { // As above, create a global instance to register your init method
        global_init();
        virtual void init(int argc, const char** argv) = 0;
    };

    struct test_stack_entry { // the stack consists of the current test case at the bottom, then subtests, and the current check at the top
        const char* name = nullptr;
        const char* file = nullptr;
        int line = -1;
        const char* context = nullptr;
    };
    struct test_stack_view { const test_stack_entry* it; const test_stack_entry* end; };
    test_stack_view test_stack();


namespace detail {
    struct test_case_registrar {
        test_case_registrar(void(*test)(), const char* name, const char* file, int line);
    };
    struct test_series_registrar {
        test_series_registrar(void(*test)(int start_index, int end_index), const char* name, const char* file, int line);
    };

    void begin_series_test(int index);
    void end_series_test();
    void begin_subtest(const char* name, const char* file, int line, const char* context = nullptr);
    void end_subtest();

    void fail_check(const char* exp, const char* context = nullptr);
    void fail_compare(const char* lhs, const char* rhs, const char* lhs_exp, const char* rhs_exp, const char* op);

    struct string_holder { // this is a kludge to avoid having to always include <string> here, as that has a noticable compile time impact
        void* s = nullptr;
        string_holder(void* stdstr) : s(stdstr) {}
        ~string_holder();
        const char* data() const;
    };

    string_holder koutest_debug_string(const void* ptr);
    string_holder koutest_debug_string(const char* chars);
    template<typename T> string_holder koutest_debug_string(const T* ptr) { return koutest_debug_string(static_cast<const void*>(ptr)); }
    string_holder koutest_debug_string(std::nullptr_t);
    string_holder koutest_debug_string(char c);
    string_holder koutest_debug_string(signed char c);
    string_holder koutest_debug_string(short i);
    string_holder koutest_debug_string(int value);
    string_holder koutest_debug_string(long value);
    string_holder koutest_debug_string(long long value);
    string_holder koutest_debug_string(unsigned char c);
    string_holder koutest_debug_string(unsigned short u);
    string_holder koutest_debug_string(unsigned value);
    string_holder koutest_debug_string(unsigned long value);
    string_holder koutest_debug_string(unsigned long long value);
    string_holder koutest_debug_string(float value);
    string_holder koutest_debug_string(double value);
#ifdef KOUTEST_STD_STRING_HANDLING
    inline std::string koutest_debug_string(std::string s) { return s; }
#endif

    template<typename T1, typename T2>
    void fail_cmp(const T1& lhs, const T2& rhs, const char* lhs_exp, const char* rhs_exp, const char* op)
    {
        fail_compare(koutest_debug_string(lhs).data(), koutest_debug_string(rhs).data(), lhs_exp, rhs_exp, op);
    }

    template<typename T1, typename T2>
    void check_eq(const T1& lhs, const T2& rhs, const char* lhs_exp, const char* rhs_exp)
    {
        if( !(lhs == rhs) ) fail_cmp(lhs, rhs, lhs_exp, rhs_exp, "==");
    }
    template<typename T1, typename T2>
    void check_ne(const T1& lhs, const T2& rhs, const char* lhs_exp, const char* rhs_exp)
    {
        if( !(lhs != rhs) ) fail_cmp(lhs, rhs, lhs_exp, rhs_exp, "!=");
    }
    template<typename T1, typename T2>
    void check_lt(const T1& lhs, const T2& rhs, const char* lhs_exp, const char* rhs_exp)
    {
        if( !(lhs < rhs) ) fail_cmp(lhs, rhs, lhs_exp, rhs_exp, "<");
    }
    template<typename T1, typename T2>
    void check_gt(const T1& lhs, const T2& rhs, const char* lhs_exp, const char* rhs_exp)
    {
        if( !(lhs > rhs) ) fail_cmp(lhs, rhs, lhs_exp, rhs_exp, ">");
    }
    template<typename T1, typename T2>
    void check_le(const T1& lhs, const T2& rhs, const char* lhs_exp, const char* rhs_exp)
    {
        if( !(lhs <= rhs) ) fail_cmp(lhs, rhs, lhs_exp, rhs_exp, "<=");
    }
    template<typename T1, typename T2>
    void check_ge(const T1& lhs, const T2& rhs, const char* lhs_exp, const char* rhs_exp)
    {
        if( !(lhs >= rhs) ) fail_cmp(lhs, rhs, lhs_exp, rhs_exp, ">=");
    }

    extern std::atomic<const char*> last_check_file; // these two values are updated before each check is evaluated, so that a crash in the evaluation
    extern std::atomic<int> last_check_line;         // can report (via a signal handler - thus they need to be atomic) an approximate location
} // namespace detail

} // namespace koutest

#define KOUTEST_DETAIL_SUBTEST_TEMPLATE(name, context, call_stm)                                   \
    do {                                                                                           \
        koutest::detail::begin_subtest(#name, __FILE__, __LINE__, context);                        \
        call_stm;                                                                                  \
        koutest::detail::end_subtest();                                                            \
    } while(false)

#define KOUTEST_DETAIL_UPDATE_LAST(file, line)                                                     \
    koutest::detail::last_check_file.store(file);                                                  \
    koutest::detail::last_check_line.store(line);

#define KOUTEST_DETAIL_CHECK_CMP(lhs, rhs, cmp)                                                    \
    do {                                                                                           \
        KOUTEST_DETAIL_UPDATE_LAST(__FILE__, __LINE__)                                             \
        koutest::detail::check_ ## cmp(lhs, rhs, #lhs, #rhs);                                      \
    } while(false)


// "public" macros:

#define TEST_CASE(name)                                                                            \
    static void name();                                                                            \
    namespace koutest { namespace tests {                                                          \
    static const detail::test_case_registrar name(::name, #name, __FILE__, __LINE__);              \
    } }                                                                                            \
    static void name()

#define TEST_SERIES(name, generator, element)                                                      \
    template<typename T> static void name(const T& element);                                       \
    namespace koutest { namespace series {                                                         \
    static void name(int start_index, int end_index) {                                             \
        const auto data = generator;                                                               \
        int index = 0;                                                                             \
        auto it = data.begin(), end = data.end();                                                  \
        for( ; index < start_index && it != end; ++it, ++index ) {}                                \
        for( ; it != end && index < end_index; ++it, ++index ) {                                   \
            detail::begin_series_test(index);                                                      \
            ::name(*it);                                                                           \
            detail::end_series_test();                                                             \
        }                                                                                          \
    }                                                                                              \
    } namespace tests {                                                                            \
    static const detail::test_series_registrar name(series::name, #name, __FILE__, __LINE__);      \
    } }                                                                                            \
    template<typename T> static void name(const T& element)

#define SUBTEST(name) KOUTEST_DETAIL_SUBTEST_TEMPLATE(name, nullptr, name())
#define SUBTEST_ARGS(name, ...) KOUTEST_DETAIL_SUBTEST_TEMPLATE(name, nullptr, name(__VA_ARGS__))
#define SUBTEST_CTX(name, context) KOUTEST_DETAIL_SUBTEST_TEMPLATE(name, context, name())
#define SUBTEST_CTX_ARGS(name, context, ...) KOUTEST_DETAIL_SUBTEST_TEMPLATE(name, context, name(__VA_ARGS__))

#define CHECK_CTX(expr, context)                                                                   \
    do {                                                                                           \
        KOUTEST_DETAIL_UPDATE_LAST(__FILE__, __LINE__)                                             \
        if( !(expr) ) koutest::detail::fail_check(#expr, context);                                 \
    } while(false)

#define CHECK(expr) CHECK_CTX(expr, nullptr)

#define CHECK_EQ(lhs, rhs) KOUTEST_DETAIL_CHECK_CMP(lhs, rhs, eq)
#define CHECK_NOT_EQ(lhs, rhs) KOUTEST_DETAIL_CHECK_CMP(lhs, rhs, ne)
#define CHECK_LESS(lhs, rhs) KOUTEST_DETAIL_CHECK_CMP(lhs, rhs, lt)
#define CHECK_GREATER(lhs, rhs) KOUTEST_DETAIL_CHECK_CMP(lhs, rhs, gt)
#define CHECK_LESS_EQ(lhs, rhs) KOUTEST_DETAIL_CHECK_CMP(lhs, rhs, le)
#define CHECK_GREATER_EQ(lhs, rhs) KOUTEST_DETAIL_CHECK_CMP(lhs, rhs, ge)

#define FAIL(msg)                                                                                  \
    do {                                                                                           \
        KOUTEST_DETAIL_UPDATE_LAST(__FILE__, __LINE__)                                             \
        koutest::detail::fail_check(msg, nullptr);                                                 \
    } while(false)

#endif // KOUTEST_H

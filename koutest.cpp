/* Copyright 2021 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <koutest.h>

#include <chrono>
#include <cstdlib>
#include <cstring>
#include <limits>
#include <memory>
#include <string>
#include <vector>

#if defined(__cpp_exceptions) || defined(__EXCEPTIONS)
#define KOUTEST_HAVE_EXCEPTIONS
#include <exception>
#endif

#include <signal.h>
#include <stdio.h>
#include <unistd.h>

namespace koutest {

using test_case_t = void(*)();
using test_series_t = void(*)(int, int);

struct test_info {
    const char* name = nullptr;
    const char* file = nullptr;
    int line = -1;
    test_case_t t = nullptr;
    test_series_t s = nullptr;
    int s_start = 0;
    int s_end = std::numeric_limits<int>().max();
};

// "private" global state

static std::vector<test_info>& s_all_tests()
{
    static std::vector<test_info> list;
    return list;
}
static callbacks* s_callbacks = nullptr;
static global_init* s_global_init = nullptr;

static std::vector<test_stack_entry> s_test_stack;
static std::vector<std::unique_ptr<std::string>> s_test_stack_contexts;
static bool s_continue_after_fail = false;
static bool s_did_fail = false;

static std::atomic_bool s_inside_callback;
static std::atomic<const char*> s_last_test(nullptr);
static std::atomic<int> s_last_series_index(-1);
static char s_signal_text_buffer[4096] = {};

// signal handling

static const int intercepted_signals[] = { SIGTERM, SIGINT, SIGQUIT,
                                           SIGABRT, SIGPIPE, SIGBUS, SIGFPE, SIGILL, SIGSEGV, SIGSYS };

static const char* safe_load_str(const std::atomic<const char*>& s)
{
    auto res = s.load();
    return res == nullptr ? "(none)" : res;
}

struct int_string { char data[3 * sizeof(int) + 2] = {}; };
static int_string sigsafe_int_to_string(int i) // printf is not signal safe, so sadly need to implement ourself
{
    int_string res;
    size_t pos = sizeof(res.data) - 2;
    if( i == 0 ) {
        res.data[pos] = '0';
        --pos;
    } else {
        unsigned abs = (i < 0) ? (unsigned(-(i + 1)) + 1) : unsigned(i);
        for( ; abs > 0; --pos ) {
            res.data[pos] = '0' + (abs % 10);
            abs /= 10;
        }
        if( i < 0 ) {
            res.data[pos] = '-';
            --pos;
        }
    }
    std::memmove(res.data, (res.data + pos + 1), (sizeof(res.data) - pos - 1));
    return res;
}

static void signal_handler(int sig)
{
    // restore default signal handler
    struct sigaction act = {};
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
    act.sa_handler = SIG_DFL;
    for( int i = 0; i < int(sizeof(intercepted_signals) / sizeof(int)); ++i )
        sigaction(intercepted_signals[i], &act, nullptr);

    const char* signame = "???";
    switch( sig ) {
    case SIGTERM: signame = "TERM (general termination request)"; break;
    case SIGINT:  signame = "INT (terminal termination request)"; break;
    case SIGQUIT: signame = "QUIT (strong termination request)"; break;
    case SIGABRT: signame = "ABRT (abort)"; break;
    case SIGPIPE: signame = "PIPE (broken pipe)"; break;
    case SIGBUS:  signame = "BUS (bus error)"; break;
    case SIGFPE:  signame = "FPE (math error, usually integer zero division)"; break;
    case SIGILL:  signame = "ILL (illegal instruction)"; break;
    case SIGSEGV: signame = "SEGV (segmentation violation)"; break;
    case SIGSYS:  signame = "SYS (bad syscall)"; break;
    }

    s_inside_callback.store(true);
    s_callbacks->signal(sig, signame, safe_load_str(detail::last_check_file), detail::last_check_line.load());

    raise(sig);
}

static void install_signal_handler()
{
    stack_t stack; // use an alternate stack for the signal handler to be able to handle stack overflow
    stack.ss_flags = 0;
    stack.ss_size = 4 * size_t(std::max<long>(SIGSTKSZ, 4096)); // a bit more on the safe side
    stack.ss_sp = malloc(stack.ss_size);
    sigaltstack(&stack, nullptr);

    struct sigaction act = {};
    sigfillset(&act.sa_mask);
    act.sa_flags = SA_ONSTACK;
    act.sa_handler = signal_handler;

    for( int i = 0; i < int(sizeof(intercepted_signals) / sizeof(int)); ++i )
        sigaction(intercepted_signals[i], &act, nullptr);
}

static size_t signal_buffer_append(size_t pos, const char* text)
{
    const auto n = std::strlen(text);
    if( n >= sizeof(s_signal_text_buffer) - pos )
        return sizeof(s_signal_text_buffer);
    std::memcpy(s_signal_text_buffer + pos, text, n);
    return pos + n;
}

static void print_signal_info(int sig, const char* signame,
                              const char* last_check_file, int last_check_line)
{
    const bool user_abort = (sig == SIGTERM || sig == SIGINT || sig == SIGQUIT);

    size_t pos = signal_buffer_append(0, user_abort ? "! User abort signal received: " : "! Error signal received: ");
    pos = signal_buffer_append(pos, signame);
    pos = signal_buffer_append(pos, "\n");
    write(STDOUT_FILENO, s_signal_text_buffer, pos);

    std::memset(s_signal_text_buffer, 0, sizeof(s_signal_text_buffer));
    pos = signal_buffer_append(0, "! Aborting signal info for ");
    pos = signal_buffer_append(pos, signame);
    pos = signal_buffer_append(pos, ":\n    Last seen test case: ");
    pos = signal_buffer_append(pos, safe_load_str(s_last_test));
    if( s_last_series_index.load() >= 0 ) {
        pos = signal_buffer_append(pos, ":");
        pos = signal_buffer_append(pos, sigsafe_int_to_string(s_last_series_index.load()).data);
    }
    pos = signal_buffer_append(pos,  "\n    Last seen check location: ");
    pos = signal_buffer_append(pos, last_check_file);
    pos = signal_buffer_append(pos,  " : ");
    pos = signal_buffer_append(pos, sigsafe_int_to_string(last_check_line).data);
    pos = signal_buffer_append(pos, "\n");
    write(STDERR_FILENO, s_signal_text_buffer, pos);
}

static void internal_assert(bool a)
{
    if( a )
        return;
    static const char msg[] = "!! Internal koutest error detected - either bug or usage error.\n";
    write(STDERR_FILENO, msg, std::strlen(msg));
    std::exit(EXIT_FAILURE);
}

// callbacks

callbacks::callbacks() { internal_assert(s_callbacks == nullptr); s_callbacks = this; }
void callbacks::begin_test_case(const char*, int) {}
void callbacks::end_test_case() {}
void callbacks::begin_subtest(const char*) {}
void callbacks::end_subtest() {}
void callbacks::check_failed(const char*) {}
void callbacks::check_cmp_failed(const char*, const char*, const char*, const char*, const char*) {}
void callbacks::exception(const char*, const char*, int) {}
void callbacks::signal(int sig, const char* signame, const char* last_check_file, int last_check_line)
{
    print_signal_info(sig, signame, last_check_file, last_check_line);
}

struct default_callbacks final : public callbacks {
    int current_test_index = -1;
    bool current_test_ok = true;
    std::chrono::time_point<std::chrono::steady_clock> current_test_start;

    void print_test_status(bool success)
    {
        auto view = test_stack();
        internal_assert(view.it != view.end);
        std::string name(view.it->name);
        if( current_test_index >= 0 )
            name = name + ":" + sigsafe_int_to_string(current_test_index).data;

        if( current_test_ok ) { // only print out one status line per test
            const auto duration = std::chrono::steady_clock::now() - current_test_start;
            const long long ms = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
            printf("> %40s : %s (%lld ms)\n", name.data(), success ? "  OK" : "FAIL", ms);
            fflush(stdout);
            current_test_ok = success;
        }
        if( success )
            return;

        // print test stack for failure to stderr
        fprintf(stderr, "%s (%s : %d)\n", name.data(), view.it->file, view.it->line);
        ++view.it;
        std::string indent;
        for( ; view.it != view.end; ++view.it ) {
            indent += "  ";
            std::string tmp(view.it->name);
            if( view.it->context != nullptr )
                tmp = tmp + "; " + view.it->context;
            fprintf(stderr, "%s%s (%s : %d)\n", indent.data(), tmp.data(), view.it->file, view.it->line);
        }
    }

    void begin_test_case(const char*, int series_index) override
    {
        current_test_index = series_index;
        current_test_ok = true;
        current_test_start = std::chrono::steady_clock::now();
    }
    void end_test_case() override
    {
        if( current_test_ok )
            print_test_status(true);
    }
    void check_failed(const char* exp) override
    {
        print_test_status(false);
        fprintf(stderr, "-> Failed: %s\n", exp);
        fflush(stderr);
    }
    void check_cmp_failed(const char* lhs, const char* rhs,
                          const char* lhs_exp, const char* rhs_exp, const char* op) override
    {
        print_test_status(false);
        fprintf(stderr, "-> Failed: (%s) %s (%s)\n   Actual lhs: %s\n   Actual rhs: %s\n",
                lhs_exp, op, rhs_exp, lhs, rhs);
        fflush(stderr);
    }
    void exception(const char* what, const char* last_check_file, int last_check_line) override
    {
        print_test_status(false);
        fprintf(stderr, "-> Exception (%s) - Last check location: %s : %i\n",
                what, last_check_file, last_check_line);
        fflush(stderr);
    }
};

global_init::global_init() { internal_assert(s_global_init == nullptr); s_global_init = this; }

test_stack_view test_stack() { return {s_test_stack.data(), s_test_stack.data() + s_test_stack.size()}; }


namespace detail {

std::atomic<const char*> last_check_file(nullptr);
std::atomic<int> last_check_line(-1);

test_case_registrar::test_case_registrar(test_case_t test, const char* name, const char* file, int line)
{
    internal_assert(test != nullptr);
    s_all_tests().push_back({name, file, line, test, nullptr});
}
test_series_registrar::test_series_registrar(test_series_t test, const char* name, const char* file, int line)
{
    internal_assert(test != nullptr);
    s_all_tests().push_back({name, file, line, nullptr, test});
}

#define CB(name)                        \
    do {                                \
        s_inside_callback.store(true);  \
        s_callbacks->name();            \
        s_inside_callback.store(false); \
    } while( false )

#define CB_ARGS(name, ...)              \
    do {                                \
        s_inside_callback.store(true);  \
        s_callbacks->name(__VA_ARGS__); \
        s_inside_callback.store(false); \
    } while( false )

static void run_test_case(test_case_t t, const char* name)
{
    CB_ARGS(begin_test_case, name, -1);
    t();
    CB(end_test_case);
}

void begin_series_test(int index)
{
    internal_assert(!s_inside_callback.load() && s_test_stack.size() == 1);
    s_last_series_index.store(index);
    CB_ARGS(begin_test_case, s_test_stack.front().name, index);
}
void end_series_test()
{
    internal_assert(!s_inside_callback.load() && s_test_stack.size() == 1);
    CB(end_test_case);
    s_last_series_index.store(-1);
}
void begin_subtest(const char* name, const char* file, int line, const char* context)
{
    internal_assert(!s_inside_callback.load() && s_test_stack.size() > 0);
    if( context != nullptr ) { // make copy of context, as it could be a temporary buffer
        s_test_stack_contexts.emplace_back(new std::string(context));
        context = s_test_stack_contexts.back()->data();
    }
    s_test_stack.push_back({name, file, line, context});
    CB_ARGS(begin_subtest, name);
}
void end_subtest()
{
    internal_assert(!s_inside_callback.load() && s_test_stack.size() > 1);
    CB(end_subtest);
    if( s_test_stack.back().context != nullptr ) {
        internal_assert(s_test_stack.back().context == s_test_stack_contexts.back()->data());
        s_test_stack_contexts.pop_back();
    }
    s_test_stack.pop_back();
}

static void fail_action()
{
    s_did_fail = true;
    if( !s_continue_after_fail )
        std::exit(EXIT_FAILURE);
}

void fail_check(const char* exp, const char* context)
{
    internal_assert(!s_inside_callback.load() && s_test_stack.size() > 0);
    s_test_stack.push_back({"CHECK", last_check_file.load(), last_check_line.load(), context});
    CB_ARGS(check_failed, exp);
    s_test_stack.pop_back();
    fail_action();
}
void fail_compare(const char* lhs, const char* rhs, const char* lhs_exp, const char* rhs_exp, const char* op)
{
    internal_assert(!s_inside_callback.load() && s_test_stack.size() > 0);
    s_test_stack.push_back({op, last_check_file.load(), last_check_line.load(), nullptr});
    CB_ARGS(check_cmp_failed, lhs, rhs, lhs_exp, rhs_exp, op);
    s_test_stack.pop_back();
    fail_action();
}

struct ptr_hex { char data[2 * sizeof(void*) + 1] = {}; };
static ptr_hex ptr_to_hexstring(const void* ptr)
{
    static const char hex[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
    ptr_hex res;
    uintptr_t uptr;
    std::memcpy(&uptr, &ptr, sizeof(ptr));
    for( int i = sizeof(ptr_hex) - 2; i >= 0; --i ) {
        res.data[i] = hex[uptr % 16];
        uptr /= 16;
    }
    return res;
}

string_holder::~string_holder() { delete static_cast<std::string*>(s); }
const char* string_holder::data() const { return static_cast<std::string*>(s)->data(); }

static string_holder wrap_string(std::string s)
{
    return string_holder(new std::string(std::move(s)));
}

string_holder koutest_debug_string(const void* ptr)
{
    return wrap_string(std::string("ptr(0x") + ptr_to_hexstring(ptr).data + ")");
}
string_holder koutest_debug_string(const char* chars)
{
    auto s = std::string("chars(0x") + ptr_to_hexstring(chars).data;
    if( chars != nullptr )
        s = s + ", <" + chars + ">";
    return wrap_string(s + ")");
}
string_holder koutest_debug_string(std::nullptr_t) { return wrap_string("nullptr"); }
string_holder koutest_debug_string(char c)
{
    const char s[] = {'\'', c, '\'', 0};
    return wrap_string(s);
}
string_holder koutest_debug_string(signed char value) { return koutest_debug_string(static_cast<int>(value)); }
string_holder koutest_debug_string(short value) { return koutest_debug_string(static_cast<int>(value)); }
string_holder koutest_debug_string(int value) { return wrap_string(std::to_string(value)); }
string_holder koutest_debug_string(long value) { return wrap_string(std::to_string(value)); }
string_holder koutest_debug_string(long long value) { return wrap_string(std::to_string(value)); }
string_holder koutest_debug_string(unsigned char value) { return koutest_debug_string(static_cast<unsigned>(value)); }
string_holder koutest_debug_string(unsigned short value) { return koutest_debug_string(static_cast<unsigned>(value)); }
string_holder koutest_debug_string(unsigned value) { return wrap_string(std::to_string(value)); }
string_holder koutest_debug_string(unsigned long value) { return wrap_string(std::to_string(value)); }
string_holder koutest_debug_string(unsigned long long value) { return wrap_string(std::to_string(value)); }
string_holder koutest_debug_string(float value) { return wrap_string(std::to_string(value)); }
string_holder koutest_debug_string(double value) { return wrap_string(std::to_string(value)); }

static void handle_exception(const char* what)
{
    internal_assert(!s_inside_callback.load() && s_test_stack.size() > 0);
    CB_ARGS(exception, what == nullptr ? "" : what, safe_load_str(last_check_file), last_check_line.load());
    fail_action();
    s_test_stack.clear();
    s_test_stack_contexts.clear();
    s_last_series_index.store(-1);
}

#undef CB
#undef CB_ARGS

} // namespace detail
} // namespace koutest

static int print_usage(const char* err, const char* prog)
{
    fprintf(stderr, "%s\n"
                    "Usage: %s -l\n"
                    "       %s [-c] [testname[:series_start[:series_end]]]... [-- ignored_args...]\n",
            err, prog, prog);
    return EXIT_FAILURE;
}

int main(int argc, const char** argv)
{
    using namespace koutest;
    std::vector<test_info> scheduled_tests;

    // Parse args

    for( int i = 1; i < argc; ++i ) {
        const char* arg = argv[i];
        if( std::strcmp(arg, "--") == 0 ) {
            break;
        } else if( std::strcmp(arg, "-l") == 0 ) {
            if( i != 1 || argc != 2 )
                return print_usage("-l cannot be used with other arguments", argv[0]);
            for( const auto& t : s_all_tests() )
                printf("%s%s\n", t.name, (t.s != nullptr ? " (series)" : ""));
            return EXIT_SUCCESS;
        } else if( std::strcmp(arg, "-c") == 0 ) {
            if( i != 1 )
                return print_usage("-c must be the first argument", argv[0]);
            s_continue_after_fail = true;
            continue;
        } else if( arg[0] == '-' ) {
            return print_usage((std::string("Unknown option ") + arg).data(), argv[0]);
        }

        const auto N = std::strlen(arg);
        size_t colon = 0;
        for( ; colon < N && arg[colon] != ':'; ++colon ) {}
        std::string name(arg, colon);
        bool found = false;
        for( const auto& t : s_all_tests() ) {
            if( name == t.name ) {
                scheduled_tests.push_back(t);
                found = true;
                break;
            }
        }
        if( !found )
            return print_usage((std::string("Invalid test: ") + name).data(), argv[0]);
        if( colon < N ) {
            if( scheduled_tests.back().s == nullptr )
                return print_usage((std::string("Test is not a series: ") + name).data(), argv[0]);
            int start = std::atoi(arg + colon + 1);
            int end = start + 1;
            for( ++colon; colon < N && arg[colon] != ':'; ++colon ) {}
            if( colon < N )
                end = std::atoi(arg + colon + 1);
            scheduled_tests.back().s_start = start;
            scheduled_tests.back().s_end = end;
        }
    }

    // Prepare running tests

    if( scheduled_tests.empty() )
        scheduled_tests = s_all_tests();

    std::unique_ptr<default_callbacks> default_cb;
    if( s_callbacks == nullptr )
        default_cb.reset(new default_callbacks());

    install_signal_handler();

    if( s_global_init != nullptr )
        s_global_init->init(argc, argv);

    // Run tests

    for( const auto& t : scheduled_tests ) {
        internal_assert(!s_inside_callback.load() && s_test_stack.empty());
        s_test_stack.push_back({t.name, t.file, t.line, nullptr});
        s_last_test.store(t.name);
        detail::last_check_file.store(nullptr);
        detail::last_check_line.store(-1);

#ifdef KOUTEST_HAVE_EXCEPTIONS
    try {
#endif
        if( t.t )
            detail::run_test_case(t.t, t.name);
        else
            t.s(t.s_start, t.s_end);

        internal_assert(s_test_stack.size() == 1);
        s_test_stack.pop_back();

#ifdef KOUTEST_HAVE_EXCEPTIONS
    } catch(const std::exception& e) {
        detail::handle_exception(e.what());
    } catch(...) {
        detail::handle_exception(nullptr);
    }
#endif
    }

    return s_did_fail ? EXIT_FAILURE : EXIT_SUCCESS;
}

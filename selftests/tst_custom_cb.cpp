#include <koutest.h>

#include <cstdlib>
#include <cstring>
#include <vector>

#include <stdio.h>

static void verify(bool a) { if( !a ) std::abort(); }

struct callbacks : public koutest::callbacks {
    ~callbacks()
    {
        verify(executed_tests == 7);
    }

    void begin_test_case(const char* name, int series_index)
    {
        verify(test_case == nullptr);
        verify(subtest == nullptr);
        verify(index == -2);
        test_case = name;
        index = series_index;
        ++executed_tests;
    }
    void end_test_case()
    {
        verify(test_case != nullptr);
        verify(subtest == nullptr);
        verify(index != -2);
        printf("%s:%d OK\n", test_case, index);
        test_case = nullptr;
        index = -2;
    }
    void begin_subtest(const char* name)
    {
        verify(test_case != nullptr);
        verify(subtest == nullptr);
        verify(index != -2);
        subtest = name;
    }
    void end_subtest()
    {
        verify(test_case != nullptr);
        verify(index != -2);
        subtest = nullptr;
    }

    const char* test_case = nullptr;
    const char* subtest = nullptr;
    int index = -2;

    int executed_tests = 0;
};
static callbacks cb;

static void sub()
{
    CHECK(std::strcmp(cb.test_case, "custom_cb") == 0);
    CHECK_EQ(cb.index, -1);
    CHECK(std::strcmp(cb.subtest, "sub") == 0);
}

TEST_CASE(custom_cb) {
    CHECK(std::strcmp(cb.test_case, "custom_cb") == 0);
    CHECK_EQ(cb.index, -1);
    CHECK(cb.subtest == nullptr);
    SUBTEST(sub);
    CHECK(cb.subtest == nullptr);
}

TEST_SERIES(custom_cb_series, std::vector<int>({0, 1, 2, 3, 4}), i) {
    CHECK(std::strcmp(cb.test_case, "custom_cb_series") == 0);
    CHECK_EQ(cb.index, i);
    CHECK(cb.subtest == nullptr);
}

TEST_CASE(custom_cb2) {
    CHECK(std::strcmp(cb.test_case, "custom_cb2") == 0);
    CHECK_EQ(cb.index, -1);
    CHECK(cb.subtest == nullptr);
}

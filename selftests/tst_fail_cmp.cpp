#include <koutest.h>

#include <cstdlib>
#include <cstring>

#include <signal.h>
#include <stdio.h>

static void verify(bool a) { if( !a ) std::abort(); }
static void verify_strcmp(const char* s1, const char* s2) { verify(std::strcmp(s1, s2) == 0); }

struct callbacks : public koutest::callbacks {
    void check_cmp_failed(const char* lhs, const char* rhs, const char* lhs_exp, const char* rhs_exp, const char* op) override
    {
        verify_strcmp(op, "==");
        verify_strcmp(lhs, "2");
        verify_strcmp(rhs, "3");
        verify_strcmp(lhs_exp, "1 + 1");
        verify_strcmp(rhs_exp, "3");

        auto s = koutest::test_stack();
        verify(s.it != s.end);
        verify_strcmp(s.it->name, "fail_cmp");
        ++s.it;
        verify(s.it != s.end);
        verify_strcmp(s.it->name, "sub");
        ++s.it;
        verify(s.it != s.end);
        verify_strcmp(s.it->name, "==");
        ++s.it;
        verify(s.it == s.end);

        printf("mark\n");
        std::exit(EXIT_SUCCESS);
    }
};
static callbacks cb;

static void sub()
{
    CHECK_EQ(1 + 1, 3);
    raise(SIGSEGV);
}

TEST_CASE(fail_cmp) {
    SUBTEST(sub);
}

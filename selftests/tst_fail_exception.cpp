#include <koutest.h>

#include <cstdlib>
#include <cstring>
#if defined(__cpp_exceptions) || defined(__EXCEPTIONS)
#include <stdexcept>
#endif

#include <stdio.h>

static void verify(bool a) { if( !a ) std::abort(); }
static void verify_strcmp(const char* s1, const char* s2) { verify(std::strcmp(s1, s2) == 0); }

static int line_before_throwing_check = -1;

struct callbacks : public koutest::callbacks {
    void exception(const char* what, const char* last_check_file, int last_check_line) override
    {
        verify_strcmp(what, "mark");
        verify_strcmp(last_check_file, __FILE__);
        verify(last_check_line == line_before_throwing_check + 1);

        auto s = koutest::test_stack();
        verify(s.it != s.end);
        verify_strcmp(s.it->name, "fail_execption");
        ++s.it;
        verify(s.it != s.end);
        verify_strcmp(s.it->name, "sub");
        ++s.it;
        verify(s.it == s.end);

        printf("mark\n");
        std::exit(EXIT_SUCCESS);
    }
};
static callbacks cb;

static bool do_throw()
{
#if defined(__cpp_exceptions) || defined(__EXCEPTIONS)
    throw std::domain_error("mark");
#endif
    return false;
}

static void sub()
{
    line_before_throwing_check = __LINE__;
    CHECK(do_throw());
}

TEST_CASE(fail_execption) {
    SUBTEST(sub);
}

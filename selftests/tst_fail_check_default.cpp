#include <koutest.h>

#include <signal.h>

TEST_CASE(fail_check_with_default_handler) {
    CHECK(false);
    // code after the check should not be executed normally, so make sure
    // we terminate with a different error code if it still get here somehow
    raise(SIGKILL);
}

#include <koutest.h>

#include <cstdlib>
#include <cstring>

#include <signal.h>
#include <stdio.h>

static void verify(bool a) { if( !a ) std::abort(); }
static void verify_strcmp(const char* s1, const char* s2) { verify(std::strcmp(s1, s2) == 0); }

static bool first = true;

struct callbacks : public koutest::callbacks {
    void check_failed(const char* exp) override
    {
        if( first ) {
            verify_strcmp(exp, "false");
            first = false;
        } else {
            verify_strcmp(exp, "zzz");
            printf("mark\n");
            std::exit(EXIT_SUCCESS);
        }
    }
};
static callbacks cb;

static void sub()
{
    CHECK(false);
}

TEST_CASE(fail_continue) {
    SUBTEST_CTX(sub, "abc");
    FAIL("zzz");
    raise(SIGKILL);
}

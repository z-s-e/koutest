#include <koutest.h>

#include <cstdlib>
#include <cstring>

#include <signal.h>
#include <unistd.h>

static int divisor = 2;
static int line_before_raising_check = -1;

struct global_init : public koutest::global_init {
    void init(int argc, const char**) override
    {
        if( argc == 1 )
            divisor = 0;
    }
};
static global_init init;

struct callbacks : public koutest::callbacks {
    void signal(int sig, const char* signame, const char* last_check_file, int last_check_line) override
    {
        if( sig != SIGFPE )
            return;
        if( std::strcmp(signame, "FPE (math error, usually integer zero division)") != 0 )
            return;
        if( std::strcmp(last_check_file, __FILE__) != 0 )
            return;
        if( last_check_line != (line_before_raising_check + 1) )
            return;
        static const char msg[] = "mark\n";
        write(STDERR_FILENO, msg, std::strlen(msg));
        std::exit(EXIT_SUCCESS);
    }
};
static callbacks cb;

TEST_CASE(fail_signal_fpe) {
    line_before_raising_check = __LINE__;
    CHECK_EQ(3, 6 / divisor);
    CHECK(false);
}

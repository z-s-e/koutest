#include <koutest.h>

#include <cstdlib>
#include <cstring>

#include <signal.h>
#include <unistd.h>


struct callbacks : public koutest::callbacks {
    void signal(int sig, const char* signame, const char* last_check_file, int last_check_line) override
    {
        if( sig != SIGINT )
            return;
        if( std::strcmp(signame, "INT (terminal termination request)") != 0 )
            return;
        if( std::strcmp(last_check_file, "(none)") != 0 )
            return;
        if( last_check_line != -1 )
            return;
        static const char msg[] = "mark\n";
        write(STDERR_FILENO, msg, std::strlen(msg));
        std::exit(EXIT_SUCCESS);
    }
};
static callbacks cb;

TEST_CASE(fail_signal_int) {
    raise(SIGINT);
}

#include <koutest.h>

#include <cstdlib>
#include <cstring>

#include <signal.h>
#include <stdio.h>

static void verify(bool a) { if( !a ) std::abort(); }
static void verify_strcmp(const char* s1, const char* s2) { verify(std::strcmp(s1, s2) == 0); }

struct callbacks : public koutest::callbacks {
    void check_failed(const char* exp) override
    {
        verify_strcmp(exp, "false");

        auto s = koutest::test_stack();
        verify(s.it != s.end);
        verify_strcmp(s.it->name, "fail_check");
        ++s.it;
        verify(s.it != s.end);
        verify_strcmp(s.it->name, "sub");
        verify_strcmp(s.it->context, "abc");
        ++s.it;
        verify(s.it != s.end);
        verify_strcmp(s.it->name, "CHECK");
        ++s.it;
        verify(s.it == s.end);

        printf("mark\n");
        std::exit(EXIT_SUCCESS);
    }
};
static callbacks cb;

static void sub()
{
    CHECK(false);
    raise(SIGKILL);
}

TEST_CASE(fail_check) {
    SUBTEST_CTX(sub, "abc");
}

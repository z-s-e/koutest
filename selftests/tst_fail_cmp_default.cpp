#include <koutest.h>

#include <signal.h>

static void sub2()
{
    const char* foo = "FOO";
    CHECK_EQ(foo, nullptr);
}

static void sub()
{
    SUBTEST(sub2);
}

TEST_CASE(fail_cmp_with_default_handler) {
    SUBTEST_CTX(sub, "abc");
    raise(SIGKILL);
}

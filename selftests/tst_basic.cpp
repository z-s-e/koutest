#define KOUTEST_STD_STRING_HANDLING
#include <koutest.h>

#include <cstring>
#include <vector>

#define CHECK_STREQ(x, y) CHECK_EQ(std::string(x), std::string(y))


static void sub1_1()
{
    auto s = koutest::test_stack();
    CHECK(s.it != s.end);
    CHECK_STREQ(s.it->name, "basic_checks");
    ++s.it;
    CHECK(s.it != s.end);
    CHECK_STREQ(s.it->name, "sub1");
    ++s.it;
    CHECK(s.it != s.end);
    CHECK_STREQ(s.it->name, "sub1_1");
    ++s.it;
    CHECK_EQ(s.it, s.end);
}

static void sub1()
{
    CHECK(true);

    auto s = koutest::test_stack();
    CHECK(s.it != s.end);
    CHECK_STREQ(s.it->name, "basic_checks");
    ++s.it;
    CHECK(s.it != s.end);
    CHECK_STREQ(s.it->name, "sub1");
    ++s.it;
    CHECK_EQ(s.it, s.end);

    SUBTEST(sub1_1);
}

static void sub2(int x, bool with_context)
{
    CHECK_EQ(x, 666);

    auto s = koutest::test_stack();
    ++s.it;
    CHECK(s.it != s.end);
    CHECK_STREQ(s.it->name, "sub2");
    if( with_context ) {
        CHECK_STREQ(s.it->context, "c");
    } else {
        CHECK(s.it->context == nullptr);
    }
}

static const int line_before_basic_checks = __LINE__;
TEST_CASE(basic_checks) {
    CHECK(true);
    CHECK(3 + 8 == 11);
    CHECK_CTX(3.0 < 4.0, "a");

    CHECK_EQ(1 + 1, 2);

    void* ptr = nullptr;
    CHECK_EQ(ptr, nullptr);

    const char* chars = "x";
    CHECK_NOT_EQ(chars, nullptr);

    CHECK_GREATER(42, 23);
    CHECK_LESS(99, 100);
    CHECK_GREATER_EQ(1000.0, -1.0);
    CHECK_GREATER_EQ(0, 0);
    CHECK_LESS_EQ(123, 456);
    CHECK_LESS_EQ(0.0, 0.);

    CHECK_STREQ(koutest::detail::last_check_file.load(), __FILE__);
    CHECK_EQ(koutest::detail::last_check_line.load(), __LINE__);

    {
        auto s = koutest::test_stack();
        CHECK(s.it != s.end);
        CHECK_STREQ(s.it->name, "basic_checks");
        CHECK(std::strstr(s.it->file, "tst_basic.cpp") != nullptr);
        CHECK_EQ(s.it->line, line_before_basic_checks + 1);
        ++s.it;
        CHECK_EQ(s.it, s.end);
    }

    SUBTEST(sub1);

    {
        auto s = koutest::test_stack();
        CHECK(s.it != s.end);
        CHECK_STREQ(s.it->name, "basic_checks");
        ++s.it;
        CHECK_EQ(s.it, s.end);
    }

    SUBTEST_ARGS(sub2, 666, false);

    SUBTEST_CTX(sub1, "b");
    SUBTEST_CTX_ARGS(sub2, "c", 666, true);
}


struct series_data { int a; int b; int result; };

TEST_SERIES(basic_series, std::vector<series_data>({{1, 1, 2}, {2, 4, 6}, {20, 30, 50}}), t) {
    CHECK_EQ(t.a + t.b, t.result);
}


struct global_init : public koutest::global_init {
    void init(int, const char** argv) override
    {
        has_run = true;
        first_arg = argv[0];
    }

    bool has_run = false;
    const char* first_arg = nullptr;
};
static global_init init;

TEST_CASE(verify_init) {
    CHECK(init.has_run);
    CHECK(init.first_arg != nullptr);
}


namespace foo {
    struct bar {
        bool operator==(const bar&) const { return true; }
        bool operator!=(const bar&) const { return true; }
        bool operator<=(const bar&) const { return true; }
        bool operator>=(const bar&) const { return true; }
        bool operator<(const bar&) const { return true; }
        bool operator>(const bar&) const { return true; }
    };
    std::string koutest_debug_string(const bar&)
    {
        return "<bar>";
    }
}

TEST_CASE(custom_type_cmp) {
    foo::bar b1, b2;
    CHECK_EQ(b1, b2);
    CHECK_NOT_EQ(b1, b2);
    CHECK_LESS(b1, b2);
    CHECK_GREATER(b1, b2);
    CHECK_LESS_EQ(b1, b2);
    CHECK_GREATER_EQ(b1, b2);
}


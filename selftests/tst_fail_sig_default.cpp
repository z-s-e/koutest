#include <koutest.h>

static int divisor = 2;

struct global_init : public koutest::global_init {
    void init(int argc, const char**) override
    {
        if( argc == 1 )
            divisor = 0; // trigger SIGFPE when called without arguments
    }
};
static global_init init;

TEST_CASE(fail_signal_with_default_handler) {
    CHECK_EQ(3, 6 / divisor);
    CHECK(false);
}

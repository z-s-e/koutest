#include <koutest.h>

#include <cstdlib>
#include <cstring>

#include <signal.h>
#include <unistd.h>

static int x = 42;
static int* ptr = &x;

struct global_init : public koutest::global_init {
    void init(int argc, const char**) override
    {
        if( argc == 1 )
            ptr = nullptr;
    }
};
static global_init init;

struct callbacks : public koutest::callbacks {
    void signal(int sig, const char*, const char*, int) override
    {
        if( sig != SIGSEGV )
            return;
        static const char msg[] = "mark\n";
        write(STDERR_FILENO, msg, std::strlen(msg));
        std::exit(EXIT_SUCCESS);
    }
};
static callbacks cb;

TEST_CASE(fail_signal_segv) {
    CHECK(*ptr);
    CHECK(false);
}
